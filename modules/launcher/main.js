//Module only for Linux / needs gtk-launch

let fs = require('fs')
let path = require('path')
let modeularjs = require('modeularjs')

const dirpath = '/usr/share/applications/'

let files = []

let getDesktopName = (p) => {
    const lines = fs.readFileSync(path.join(dirpath,'./' + p)).toString().split('\n')
    for (let index = 0; index < lines.length; index++) {
        const line = lines[index];
        if (line.startsWith('Name='))return line.substring(5)
    }
    return p
}

fs.readdir(dirpath, (err, f) => {
    f.forEach(file => {
      if (file.endsWith('.desktop')){
          files.push({name:getDesktopName(file),path:file})
      }
    });
  });

function run_cmd(cmd, callBack ) {
var exec = require('child_process').exec;
var child = exec(cmd)
var resp = ''
child.stdout.on('data', function (buffer) { resp += buffer.toString() });
child.stdout.on('end', function() { callBack (resp) });
return child
} // ()

class LauncherModule extends modeularjs.ModeularJSModule{
    constructor(){
        super()
    }
    makeResult(request,obj){
        let data = obj.data
        switch(obj.request){
            case 'search':
                if (data == undefined || data == null || typeof(data) != 'string')return new modeularjs.ModeularJSResult(modeularjs.ModeularJSResult.TYPE_JSON, 'Error')  
                else{
                    data=data.toLowerCase()
                    let objects = []
                    if (data.length == 0)objects = files
                    else{
                        for (let index = 0; index < files.length; index++) {
                            const file = files[index];
                            if (file.name.toLowerCase().includes(data))objects.push(file)
                        }
                    }
                    return new modeularjs.ModeularJSResult(modeularjs.ModeularJSResult.TYPE_JSON, objects)  
                }
            case 'launch':
                    if (data == undefined || data == null || typeof(data) != 'string')return new modeularjs.ModeularJSResult(modeularjs.ModeularJSResult.TYPE_JSON, 'Error')  
                    else{
                        const apppath = path.join(dirpath, './'  +data)
                        if (fs.existsSync(apppath)){
                            run_cmd('gtk-launch ' + data, (res)=>{
                                console.log(res)
                            })
                            return new modeularjs.ModeularJSResult(modeularjs.ModeularJSResult.TYPE_JSON, 'Success')  
                        }
                        else return new modeularjs.ModeularJSResult(modeularjs.ModeularJSResult.TYPE_JSON, 'Error')  
                    }
            case undefined:
            case null:
            default:
                return new modeularjs.ModeularJSResult(modeularjs.ModeularJSResult.TYPE_JSON, 'Error')  
        }

    }
    load(){

    }
    unload(){
        
    }
}

exports.module = new LauncherModule()
