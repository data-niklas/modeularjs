let modeularjs = require('modeularjs')
let ip = require('ip')

class LocalIpModule extends modeularjs.ModeularJSModule{
    constructor(){
        super()
    }
    makeResult(req,string){
        return new modeularjs.ModeularJSResult(modeularjs.ModeularJSResult.TYPE_PLAIN,ip.address())
    }
    load(){

    }
    unload(){
        
    }
}

exports.module = new LocalIpModule()