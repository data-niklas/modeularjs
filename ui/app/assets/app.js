function log(obj){
    Neutralino.storage.putData({
        'bucket':'data',
        'content':obj
    }, ()=>{

    },()=>{

    })
}


function run(){
    Neutralino.storage.getData('data',(data)=>{
        if (data.remember){
            Swal.fire({
                'text':data.text,
                inputPlaceholder:'Remember your decision?',
                'input':'checkbox',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No',
                reverseButtons: true,
                onBeforeOpen: ()=>{
                    document.getElementsByClassName('swal2-cancel swal2-styled')[0].addEventListener('click', (e) => {
                        let remember = document.getElementById('swal2-checkbox').value
                        if (remember == 1)remember = true
                        else remember = false
                        log({'result':false,'remember':remember})
                    })
                }
            }).then(res => {
                if (res.value != null){
                    let remember = res.value
                    if (remember == 1)remember = true
                    else remember = false
                    log({'result':true,'remember':remember})
                }
                else if (res.dismiss != 'cancel'){
                    log({'result':false,'remember':false})
                }

            })
        }
        else{
            Swal.fire({
                'title':data.text,
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No',
                reverseButtons: true,
                onBeforeOpen: ()=>{
                    document.getElementsByClassName('swal2-cancel swal2-styled')[0].addEventListener('click', (e) => {
                        log({'result':false})
                    })
                }
            }).then(res => {
                
                if (res.value){
                    log({'result':true})
                }
                else if (res.dismiss != 'cancel'){
                    log({'result':false})
                }
                exit()
            })
        }
    },(err)=>{
        log({'result':false})
    })

}


Neutralino.init({
    load: function() {
        run()
    },
    pingSuccessCallback : function() {

    },
    pingFailCallback : function() {

    }
});
