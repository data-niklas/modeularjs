let modeularjs = require('modeularjs')

class PingModule extends modeularjs.ModeularJSModule{
    constructor(){
        super()
    }
    makeResult(req,string){

        return new modeularjs.ModeularJSResult(modeularjs.ModeularJSResult.TYPE_PLAIN,string)
    }
    load(){

    }
    unload(){
        
    }
}

exports.module = new PingModule()