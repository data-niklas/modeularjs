let modeularjs = require('modeularjs')
let http = require('https')

class CorsModule extends modeularjs.ModeularJSModule{
    constructor(){
        super()
    }
    makeResult(request,string){
        return new Promise((resolve, reject) => {
            http.get(string, (resp) => {
                let data = '';

                // A chunk of data has been recieved.
                resp.on('data', (chunk) => {
                    data += chunk;
                });

                // The whole response has been received. Print out the result.
                resp.on('end', () => {
                    resolve(new modeularjs.ModeularJSResult(modeularjs.ModeularJSResult.TYPE_CUSTOM,data,resp.statusCode,resp.headers))
                });

            }).on("error", (err) => {
                resolve(new modeularjs.ModeularJSResult(modeularjs.ModeularJSResult.TYPE_JSON,JSON.stringify(false)))

            });
        })
    
    }
    load(){

    }
    unload(){
        
    }
}

exports.module = new CorsModule()