#!/usr/bin/env node
const fs = require('fs')
const path = require('path')
const ModeularJS = require('./modeularjs')


var deleteFolderRecursive = function(path) {
    if( fs.existsSync(path) ) {
      fs.readdirSync(path).forEach(function(file,index){
        var curPath = path + "/" + file;
        if(fs.lstatSync(curPath).isDirectory()) { // recurse
          deleteFolderRecursive(curPath);
        } else { // delete file
          fs.unlinkSync(curPath);
        }
      });
      fs.rmdirSync(path);   
    }
  };

let getWhitelist = name => {
    let wlpath = path.join(__dirname,'./modules/' + name + '/whitelist.json')
    if (fs.existsSync(wlpath))
        return JSON.parse(fs.readFileSync(wlpath))
    else{
        fs.writeFileSync(wlpath, '[]')
        return []
    }
}

let setWhitelist = (name,whitelist) => {
    let wlpath = path.join(__dirname,'./modules/' + name + '/whitelist.json')
    fs.writeFileSync(wlpath, JSON.stringify(whitelist))
}

let getModule = name => {
    let mpath = path.join(__dirname,'./modules/' + name + '/module.json')
    if (!fs.existsSync(mpath))return false
    return JSON.parse(fs.readFileSync(mpath))
}

let args = process.argv
if (args.length > 4){
    let extra = args[4]
    switch(args[2]){
        case 'permission':
            switch(args[3]){
                case 'check':{
                    let module = getModule(extra)
                    if (module){
                        console.log('Protected:',module.protected)
                        console.log('Whitelist:',getWhitelist(extra))
                    }
                    else console.log('Module does not exist')
                    break
                }
                case 'add':{
                    let extra2 = args[5]
                    let module = getModule(extra)
                    if (module){
                        if (module.protected){
                            let whitelist = getWhitelist(extra)
                            if (whitelist.indexOf(extra2) == -1){
                                whitelist.push(extra2)
                                setWhitelist(extra,whitelist)
                                console.log('Added')
                            }
                            else console.log(extra2 + ' is already whitelisted!')
                        }
                    }
                    else console.log('Module does not exist')
                    
                    break
                }
                case 'remove':{
                    let extra2 = args[5]
                    let module = getModule(extra)
                    if (module){
                        if (module.protected){
                            let whitelist = getWhitelist(extra)
                            let index = whitelist.indexOf(extra2)
                            if (index == -1){
                                console.log(extra2 + ' was not whitelisted!')
                            }
                            else {
                                whitelist.splice(index,1)//Remove index
                                setWhitelist(extra,whitelist)
                                console.log('Removed')
                            }
                        }
                    }
                    else console.log('Module does not exist')
                    
                    break
                }

            }
            break
    }
}
else if (args.length == 3){
    switch(args[2]){
        case 'kill':
            ModeularJS.ModeularJSServer.kill()
            break
        case 'reload':
            ModeularJS.ModeularJSServer.reload()
            break  
        case 'started':
            ModeularJS.ModeularJSServer.started(bool => {
                console.log(bool)
            })
            break
    }
}
else if (args.length == 4){
    let r = args[3]
    let root = path.join(__dirname,'./modules')
    switch(args[2]){
        case 'install':
            r = path.join(process.cwd(),r)
            let mp = path.join(r,'./module.json')
            let jp = path.join(r,'./main.js')
            if (fs.existsSync(mp) && fs.existsSync(jp)){
                let obj = JSON.parse(fs.readFileSync(mp))
                let name = obj.name
                let ir = path.join(root,'./' + name)
                if (fs.existsSync(ir)){
                    console.log('Module already exists, will override it')
                }
                else{
                    fs.mkdirSync(ir)
                }
                fs.copyFile(mp, path.join(ir,'./module.json'), (err) => {
                    if (err) throw err;
                    console.log('module.json was copied');
                });
                fs.copyFile(jp, path.join(ir,'./main.js'), (err) => {
                if (err) throw err;
                    console.log('main.js was copied');
                });
            }
            else{
                console.log('Cannot install from this directory')
                console.log('Some files are missing')
                console.log('A module.json and a main.js file are needed!')
            }
            break
        case 'uninstall':
            let uninstallir = path.join(root,r)
            if (fs.existsSync(uninstallir)){
                deleteFolderRecursive(uninstallir)
                console.log('Module deleted')
            }
            else{
                console.log('Module does not exist')
            }
            break
        case 'check':

            let info = getModule(r)
            if (info){
                console.log('Name:',info.name)
                console.log('Description:',info.description)
                console.log('Version:',info.version)
                console.log('Author:',info.author)
                console.log('Protected:',info.protected)
                console.log('Whitelist:',getWhitelist(r))

            }
            else{
                console.log('Module does not exist')
            }
            break  
    }
}
else if (args.length == 2){
    ModeularJS.ModeularJSServer.started(bool => {
        if (!bool){

            let server = new ModeularJS.ModeularJSServer()
            server.start()
            console.log('ModeularJS now running')
        }
        else console.log('ModeularJS is already running!')
    })
    
    
}


