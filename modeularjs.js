#!/usr/bin/env node
const http = require('http')
const fs = require('fs')
const path = require('path')
const ip = require('ip')
const uuidv1 = require('uuid/v1')
const urltool = require('url')

const getDirectories = source =>
  fs.readdirSync(source, { withFileTypes: true })
    .filter(dirent => dirent.isDirectory())
    .map(dirent => dirent.name)


class ModeularJSServer{
        constructor(){
            this.loadModules()
            this.loadWhitelist()
            this.tempWhitelist = {}
            
            let _this = this
            process.once('SIGINT', function (code) {
                console.log('SIGINT received...');
                _this.stop();
            });
            
            // vs.
            process.once('SIGTERM', function (code) {
                console.log('SIGTERM received...');
                _this.stop();
            });
        }
        stop(){
            for (let index = 0; index < this.modules.length; index++) {
                const module = this.modules[index];
                module.unload();
            }
            this.server.close();
            process.exit();
        }
        start(){
            let _this = this
            this.server = http.createServer(function(req,res){
                console.log(req.url)             


                //Only allow localhost making requests
                if (!ip.isPrivate(req.connection.remoteAddress) ){
                    res.statusCode = 404
                    res.end('Not Allowed')
                    console.log('Not Allowed',req.connection.remoteAddress)
                    return
                }

                res.setHeader('Access-Control-Allow-Credentials', true);
                res.setHeader('Access-Control-Allow-Origin', '*');
                res.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
                res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept');

                let url = req.url
                let search = urltool.parse(req.url).query
                if (search == null)search = ''
                const searchParams = new URLSearchParams(search)        

                res.setHeader('Content-Type', 'text/plain')

                if (url == '/'){
                    res.statusCode = 200
                    let info = JSON.parse(fs.readFileSync(path.join(__dirname,'./package.json')))
                    res.setHeader('Content-Type', 'application/json')
                    res.end(JSON.stringify({
                        'name':info.name,
                        'version':info.version,
                        'description':info.description,
                        'author':info.author
                    }))
                }
                else if (url.startsWith('/permission/')){
                    _this.checkPermission(searchParams.get('module'),searchParams.get('token'),(result)=>{
                        res.setHeader('Content-Type','application/json')
                        res.end(JSON.stringify(result))
                    })
                }
                else if (url.startsWith('/askpermission/')){
                    _this.askPermission(searchParams.get('module'),searchParams.get('token'),searchParams.get('name'),(result)=>{
                        res.setHeader('Content-Type','application/json')
                        res.end(JSON.stringify(result))
                    })
                }
                else if (url.startsWith('/reload/')){
                    if (searchParams.get('pid') === String(process.pid)){
                        _this.loadModules()
                        res.end('Reloaded')
                    }
                    else res.end('Permission Denied')
                }
                else if (url.startsWith('/generate/')){
                    res.statusCode = 200
                    res.end(_this.generateRandomIdentifier())
                }
                else if (url.startsWith('/module/')){//Return if a module exists

                    res.statusCode = 200
                    res.setHeader('Content-Type', 'application/json')

                    let modulename = searchParams.get('module')
                    if (url.length == 9 || modulename == null){
                        res.end(JSON.stringify(_this.modules))
                    }
                    else{
                        res.end(JSON.stringify(_this.moduleExists(modulename)))
                    }
                }
                else {
                    let found = false
                    for (let index = 0; index < _this.modules.length; index++) {
                        const module = _this.modules[index];
                        if (url.startsWith('/' + module.name)){
                            found = true
                            let allowed = false
                            let token = searchParams.get('token')
                            let name = searchParams.get('name')
                            let data
                            try{
                                data = JSON.parse(decodeURIComponent(searchParams.get('data')))
                            }
                            catch(e){
                                data = null
                            }
                            if (token != null){
                                if (_this.hasPermission(module.name,token))allowed = true
                            }
                            if (allowed){
                                _this.sendModuleResponse(module,data,req,res)
                            }
                            else if (!_this.isDenied(module.name,token) && name != null){
                                _this.askPermission(module.name,token,name,(result)=>{
                                    if (result.result){
                                        _this.sendModuleResponse(module,data,req,res)
                                    }
                                    else{
                                        res.statusCode = 200
                                        res.setHeader('Content-Type', 'text/plain')
                                        res.end('Not allowed')
                                    }
                                })
                            }
                            else{
                                res.statusCode = 200
                                res.setHeader('Content-Type', 'text/plain')
                                res.end('Not allowed')
                            }
                            break
                        }
                    }
                    if (!found){
                        res.statusCode = 404
                        res.end()
                    }
                    
                    
                }
    
      
            });
            this.server.listen(ModeularJSServer.port)
            fs.writeFileSync(path.join(__dirname, './store'), String(process.pid));
        }
        sendModuleResponse(module,data,req,res){
            let result
            try{
                result = module.makeResult(req,data)
            }
            catch(e){
                res.setHeader('Content-Type','application/json')
                res.end(JSON.stringify(null))
                return
            }
            if (result instanceof Promise){
                result.then(finalresult => {
                    finalresult.sendResponse(res)
                }).catch(error => {
                    res.setHeader('Content-Type','application/json')
                    res.end(JSON.stringify(null))
                })
            }
            else{
                result.sendResponse(res)
            }
        }
        checkPermission(modulename,token,callback){
            if (token != null){
                if (this.isUUID(token))callback({result:false,remember:false})
                else if (!this.specialCharacters(token))callback({result:false,remember:false})
                else if (this.isDenied(modulename,token))callback({result:false,remember:true})
                else if (this.hasTemporaryPermission(modulename,token))callback({result:true,remember:false})
                else if (this.hasPermanentPermission(modulename,token))callback({result:true,remember:true})
                else callback({result:false,remember:false})
            }
            else{
                res.end(JSON.stringify(false))
            }
        }
        askPermission(modulename,token,name,callback){
            let _this = this
            if (token != null && name != null && modulename != null){
                let index = this.getModuleIndex(modulename)
                let module = this.modules[index]

                if (!this.isUUID(token))res.end('Not a UUID')
                else if (this.checkNameExists(name,token)){
                    this.askGUIYesNo('Seems like ' + name + ' generated a new token.\nRemove old token or deny access?\nOnly remove the old token, if you can be certain, that no other party tries to gain unauthorised access, by pretending to be someone else!', false,(result)=>{
                        if (result.result){
                            for (let t in _this.whitelist){
                                if (t.name == name && t != token){
                                    delete _this.whitelist[t]
                                    fs.writeFile(path.join(__dirname,'./whitelist.json'), JSON.stringify(_this.whitelist))
                                    break
                                }
                            }
                            for (let t2 in _this.tempWhitelist){
                                if (t2.name == name && t2 != token){
                                    delete _this.tempWhitelist[t2]
                                    break
                                }
                            }
                            _this.askPermission2(module.name,name,token,callback)
                        }
                        else{
                            console.log('Access denied')
                            callback({result:false,remember:false})
                        }
                    })
                }
                else this.askPermission2(module.name,name,token,callback)
            }
            else callback({result:true,remember:false})
        }
        askPermission2(modulename,name,token,callback){
            let _this = this
            if (this.isDenied(modulename,token))callback({remember:true,result:false})
            else if (!this.hasPermission(modulename,token)){
            
                this.askGUIYesNo('Allow ' + name + ' ('+token+') to access the module ' + modulename + '?', true,(result)=>{
                    if (result.result){
                        _this.addPermission(modulename,token,name,result.remember)
                        console.log('Access enabled')
            
                    }
                    else{
                        console.log('Access denied')
                        if (result.remember){
                            _this.addDenied(modulename,token,name)
                        }
                    }
                    callback(result)
                })
            }
            else callback({result:true,remember:this.hasPermanentPermission(modulename,token)})
        }
        
        addPermission(modulename,token,name,permanent){
            if (permanent){
                if (token in this.whitelist){
                    this.whitelist[token].allowed.push(modulename)
                }
                else {
                    this.whitelist[token] = {
                        'name': name,
                        'allowed':[modulename],
                        'forbidden':[]
                    }
                }
            
                fs.writeFileSync(path.join(__dirname,'./whitelist.json'), JSON.stringify(this.whitelist))
            }
            else{
                if (token in this.tempWhitelist){
                    this.tempWhitelist[token].push(modulename)
                }
                else{
                    this.tempWhitelist[token] = [modulename]
                }
            }
        }
        addDenied(modulename,token,name){
            if (token in this.whitelist){
                this.whitelist[token].forbidden.push(modulename)
            }
            else {
                this.whitelist[token] = {
                    'name': name,
                    'allowed':[],
                    'forbidden':[modulename]
                }
            }
            fs.writeFile(path.join(__dirname,'./whitelist.json'), JSON.stringify(this.whitelist))
       
        }
        isDenied(modulename,token){
            let tokeninwhitelist = this.whitelist[token]
            return (tokeninwhitelist != null && tokeninwhitelist.forbidden.indexOf(modulename)!=-1)
        }
        askGUIYesNo(text,remember,callback){//remember = If it should ask to remember changes
            
            let cmd = ''
            switch(process.platform){
                case 'win32':
                    cmd = './neutralino.exe'
                    break
                case 'linux':
                    cmd = './neutralino'
                    break
                default:
                    break
            }
            if (cmd != ''){
                let child
                let done = false

                let file = path.join(__dirname,'./ui/storage/data.json')
                fs.writeFileSync(file,
                    JSON.stringify({'text':text,'remember':remember}))
                fs.watch(file, (event,filename)=>{
                    if (filename && event ==='change') {
                        try{
                            let obj = JSON.parse(fs.readFileSync(file))
                            if (obj.text != null && obj.remember != null && !done)return
                            done = true
                            fs.unwatchFile(file)
                            child.kill()
                            callback(obj)   
                        }
                        catch(e){
                        }
                    }
                })
                child = run_cmd(cmd, out => {
                    if (done)return
                    child.kill()
                    if (remember)callback({'result':false,'remember':false})
                    else callback({'result':false})
                },path.join(__dirname,'./ui'))
            }
            else{
                return this.askConsoleYesNoRemember(text,remember,callback)
            }
        }
        askConsoleYesNoRemember(text,remember,callback){
            this.askConsoleYesNo(text,bool => {
                if (remember)this.askConsoleYesNo('Remember your decision?', bool2 => {
                    callback({'result':bool,'remember':bool2})
                })
                else callback({'result':bool})
            })
        }
        askConsoleYesNo(text,callback){
            var standard_input = process.stdin;
            standard_input.setEncoding('utf-8');
            let listener = function (data) {

                data = data.substring(0,data.length - 1)//Remove \n
                data = data.toLowerCase()
                if (data == 'y' || data == 'yes')callback(true)
                else callback(false)
                standard_input.removeListener('data',listener)
               
            }
            standard_input.on('data', listener);
            console.log(text)
        }

        getModule(name){
            for (let index = 0; index < this.modules.length; index++) {
                const module = this.modules[index];
                if (module.name == name)return module;
            }
            return null
        }
        getModuleIndex(name){
            for (let index = 0; index < this.modules.length; index++) {
                const module = this.modules[index];
                if (module.name == name)return index
            }
            return -1
        }

        samePID(pid){
            if (isNaN(pid)){
                return false
            }
            else if (Number(pid) == this.pid){
                return true
            }
            else {
                return false
            }
        }

        moduleExists(pkg){
            for (let index = 0; index < this.modules.length; index++) {
                const module = this.modules[index];
                if (module.name == pkg)return module
            }
            return false
        }

        loadModules(){
            this.modules = [];
            let root = path.join(__dirname,'./modules');
            let dirs = getDirectories(root);
            let _this = this;
            dirs.forEach(dir => {
                let p = path.join(root, dir);
                let mp = path.join(p,'/module.json');
                let jp = path.join(p,'/main.js');
                if (fs.existsSync(mp) && fs.existsSync(jp)){
                    let module = require(jp.substring(0,jp.length - 3)).module;
                    if (module != null){
                        let info = JSON.parse(fs.readFileSync(mp));
                        module.name = info.name;
                        module.version = info.version;
                        module.description = info.description;
                        module.author = info.author;
                        module.load();
                        _this.modules.push(module);
                    }
                }
            })
        }

        loadWhitelist(){
            this.whitelist = JSON.parse(fs.readFileSync(path.join(__dirname,'./whitelist.json')))
        }

        isUUID(uuid){
            return /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i.test(uuid)
        }

        generateRandomIdentifier(){
            
            return uuidv1();
             
        }

        checkNameExists(name,token){
            for (let t in this.whitelist){
                if (t.name == name && token != t){
                    return true
                }
            }
            return false
        }

        hasPermanentPermission(modulename,token){
            let obj = this.whitelist[token]
            if (obj == undefined)return false
            else return obj.allowed.indexOf(modulename) != -1
        }

        hasTemporaryPermission(modulename,token){
            let obj = this.tempWhitelist[token]
            if (obj == undefined)return false
            else return obj.indexOf(modulename) != -1
        }

        hasPermission(modulename,token){
            let res = this.hasPermanentPermission(modulename,token)
            if (!res){
                return this.hasTemporaryPermission(modulename,token)
            }
            else return true
        }

}
ModeularJSServer.port = 3001
ModeularJSServer.kill = () => {
    let pid = fs.readFileSync(path.join(__dirname, './store'), 'utf8');
    console.log('Will kill',pid)
    process.kill(Number(pid), 'SIGTERM')
}
ModeularJSServer.reload = () => {
    let pid = fs.readFileSync(path.join(__dirname, './store'), 'utf8');
    http.get('http://localhost:'+ModeularJSServer.port+'/reload/?pid='+pid, (resp) => {
        let data = '';

        // A chunk of data has been recieved.
        resp.on('data', (chunk) => {
            data += chunk;
        });

        // The whole response has been received. Print out the result.
        resp.on('end', () => {
            console.log(data);
        });

        }).on("error", (err) => {
            console.log("ModeularJS not started");
        });
}
ModeularJSServer.started = (callback) => {
    http.get('http://localhost:'+ModeularJSServer.port+'/', (resp) => {
        let data = '';

        // A chunk of data has been recieved.
        resp.on('data', (chunk) => {
            data += chunk;
        });

        // The whole response has been received. Print out the result.
        resp.on('end', () => {
            data = JSON.parse(data)
            callback(data.name === 'modeularjs')
        });

        }).on("error", (err) => {
            callback(false)
        });
}


class ModeularJSModule{
    constructor(){
        this.name = ''//Will be assigned in the loadModules() function
        this.description = ''
        this.version = ''
        this.whitelist = []
    }
    makeResult(req,obj){//Override this function; return a NodularJSResult
        return new ModeularJSResult(ModeularJSResult.TYPE_PLAIN, 'Success')
    }

}

class ModeularJSResult{
    constructor(type,value,statuscode=200,header={}){
        this.type = type
        this.value = value
        this.statuscode = statuscode
        this.header = header
    }
    sendResponse(res){
        res.statusCode = this.statuscode

        switch(this.type){
            case 0:
                res.setHeader('Content-Type', 'application/json')
                res.end(JSON.stringify(this.value))
                break
            case 1:
                res.setHeader('Content-Type', 'text/plain')
                res.end(this.value)
                break
            case 2:
                res.setHeader('Content-Type', 'text/html')
                res.end(this.value)
                break    
            case 3:
                res.writeHead(this.statuscode,this.header)
                this.value.pipe(res)
                break
            case 4:
                res.writeHead(this.statuscode,this.header)
                res.end(this.value)
                break 
        }
    }
}
ModeularJSResult.TYPE_JSON = 0
ModeularJSResult.TYPE_PLAIN = 1
ModeularJSResult.TYPE_HTML = 2
ModeularJSResult.TYPE_Piped = 3
ModeularJSResult.TYPE_CUSTOM = 4


exports.ModeularJSServer = ModeularJSServer
exports.ModeularJSModule = ModeularJSModule
exports.ModeularJSResult = ModeularJSResult


function run_cmd(cmd, callBack, cwd ) {
    var exec = require('child_process').exec;
    var child = exec(cmd,{'cwd':cwd})
    var resp = ''
    child.stdout.on('data', function (buffer) { resp += buffer.toString() });
    child.stdout.on('end', function() { callBack (resp) });
    return child
} // ()